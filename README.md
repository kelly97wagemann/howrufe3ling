# HowRUfe3linG



## About the project

HowRUfe3linG is a finishing project of the course "Massive Data Processing" from the DCC in the University of Chile. 
The main objectives are :
1. To find associations between the words used in a tweet and the predominant feeling of it.
2. To find associtations between important dates of the year and the predominant feelings of the tweets made that day. 

The data being used corresponds to the [Sentiment140  dataset](https://www.kaggle.com/datasets/kazanova/sentiment140) of kaggle, with 1.6M rows. More detailed information about the dataset, how it was generated and its possible uses can be found in the paper [Twitter Sentiment Classification using Distant Supervision](https://www-cs.stanford.edu/people/alecmgo/papers/TwitterDistantSupervision09.pdf).

To attain the objectives, it is proposed to perform queries - on the preprocessed datased - using MongoDB.


<sub><sup>Originally, the project was intended to use Hadoop, but problems arised. As a group, we decided that the best solution was to change the technology used (so we could deliver something u.u)</sup></sub> 

### Structure of the repo

* The data used can be found in the "data" folder. 
* All the code and queries made can be found in the "src" folder. 
* More extensive and explanatory results can be found in the "results" folder. 

## Techniques and technologies used

* All data preprocessing was done in Python, mostly with pandas.
* The data processing was done with MongoDB queries.

## Future work

More intensive data preprocessing is recommended so that some queries show better results (ie. more useful info). For example, removing stopwords from the text column of the dataset would have made possible to obtain better results for the "most used words in tweets with negative/positive feelings" query. 

Considering doing the same queries but with another twitter dataset - more rich and with tweets from the current year, for example - would give, possibly, more interesting outcomes. The dataset used in this project is pretty small (1.6M rows) and limited (only english language in theory (but some tweets in other languages for some reason), with only very positive o very negative feeling targets), so we recommend to work with another set of data in the future. 



## Authors
- Javier Andrews, @jaedango
- Abraham Oquiche, @abraham054
- Kelly Wagemann, @kelly97wagemann