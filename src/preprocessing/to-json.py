import pandas as pd
import os

dir_path = os.path.dirname(os.path.realpath(__file__))
df = pd.read_csv(os.path.join(dir_path, "../../data/interim/wheader-training.1600000.processed.noemoticon.csv"), encoding = "latin1")

json_data = df.to_json(orient='records')

with open(os.path.join(dir_path, "../../data/interim/wheader-training.1600000.processed.noemoticon.json"), 'w') as f:
    f.write(json_data)