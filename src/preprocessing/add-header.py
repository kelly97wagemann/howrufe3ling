import os
import pandas as pd


dir_path = os.path.dirname(os.path.realpath(__file__))
df = pd.read_csv(os.path.join(dir_path, "../../data/raw/training.1600000.processed.noemoticon.csv"), encoding = "latin1", header=None)
headerList = ["target", "ids", "date", "flag", "user", "text"]
df.to_csv("../../data/interim/wheader-training.1600000.processed.noemoticon.csv", header=headerList, index=False)
