import os
import json

# Helper function to change string to array of strings separated by space
def text_to_list(dic_list):
    for dic in dic_list:
        for key in dic.keys():
            if isinstance(dic[key], dict):
                text_to_list(dic[key])
            elif key == "text":
                dic[key] = dic[key].split(" ")


dir_path = os.path.dirname(os.path.realpath(__file__))
training_from_json = "../../data/interi,/wheader-training.1600000.processed.noemoticon.json"


with open(os.path.join(dir_path, training_from_json)) as f:
    data = json.load(f)
    text_to_list(data)
    with open(
        os.path.join(
            os.path.dirname(os.path.realpath(__file__)), 
            "../../data/processed/wheader-textlist-training.1600000.processed.noemoticon.json"
        ),
        "w",
    ) as f:
        json.dump(data, f, indent=4)
